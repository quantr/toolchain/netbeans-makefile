
import hk.quantr.javalib.CommonLib;
import hk.quantr.javalib.CommonLib.OSType;
import java.io.IOException;
import org.junit.Test;

public class TestCmd {

	@Test
	public void test() throws IOException {
		if (CommonLib.getOS() == OSType.win) {
			Runtime.getRuntime().exec("cmd /k start wsl bash -c \"cd /mnt/c/workspace/quantr-i; make clean; bash\"");
		} else if (CommonLib.getOS() == OSType.mac) {
			Runtime.getRuntime().exec("/usr/bin/osascript -e 'tell application \"Terminal\" to do script \"echo hello\"'");
		}

	}
}
