package hk.quantr.netbeans.makefile;

import hk.quantr.javalib.CommonLib;
import hk.quantr.javalib.CommonLib.OSType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Collection;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.swing.AbstractAction;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.loaders.DataFolder;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.openide.util.Utilities;
import org.openide.util.actions.Presenter;

@ActionID(
		category = "Bugtracking",
		id = "hk.quantr.netbeans.makefile.MakefileAction"
)
@ActionRegistration(
		iconBase = "hk/quantr/netbeans/makefile/makefile.png",
		displayName = "#CTL_MakefileAction",
		lazy = false
)
@ActionReferences({
	@ActionReference(path = "Menu/File", position = 0),
	@ActionReference(path = "Loaders/text/x-makefile/Actions", position = 40000),
	@ActionReference(path = "Editors/text/x-makefile/Popup", position = 40000)
})
@Messages("CTL_MakefileAction=Makefile")
public final class MakefileAction extends AbstractAction implements ActionListener, Presenter.Popup {

	File folder;

	public MakefileAction() {
	}

	@Override
	public void actionPerformed(ActionEvent ev) {
		try {
			JMenuItem menuItem = (JMenuItem) ev.getSource();
			String command = menuItem.getText();
//			System.out.println(folder.getAbsolutePath());
//			String c = "/usr/bin/osascript -e 'tell app \"Terminal\"\n"
//					+ "do script \"make " + command + "\"\n"
//					+ "activate\n"
//					+ "end tell'";
//			System.out.println(c);
//			Runtime.getRuntime().exec(c, null, folder);

			if (CommonLib.getOS() == OSType.win) {
				Runtime.getRuntime().exec("cmd /k start wsl bash -c 'cd /mnt/" + folder.getAbsolutePath().replace("C:\\", "c/").replaceAll("\\\\", "/") + "; make " + command + "; bash'");
//				Runtime.getRuntime().exec("cmd /k start wsl bash -c 'cd /mnt/c/workspace/quantr-i'");
			} else if (CommonLib.getOS() == OSType.mac) {
				String script = "tell app \"Terminal\"\n"
						+ "do script \"make " + command + "\"\n"
						+ "activate\n"
						+ "end tell'";
				ScriptEngineManager mgr = new ScriptEngineManager();
				ScriptEngine engine = mgr.getEngineByName("AppleScript");
				engine.eval(script);
			} else {
				Runtime.getRuntime().exec("cd " + folder.getAbsolutePath().replace("C:\\", "c/").replaceAll("\\\\", "/") + "; make " + command + "");
			}
		} catch (Exception ex) {
			Exceptions.printStackTrace(ex);
		}
	}

	@Override
	public JMenuItem getPopupPresenter() {
		Lookup l = Utilities.actionsGlobalContext();
		DataFolder df = l.lookup(DataFolder.class);

		Collection<? extends MakefileDataObject> matchingObjects = l.lookupAll(MakefileDataObject.class);
//		for (MakefileDataObject m : matchingObjects) {
//			System.out.println("m=" + m.getPrimaryFile().getPath());
//		}

		if (matchingObjects.size() > 0) {
			MakefileDataObject dataObject = matchingObjects.iterator().next();
			folder = new File(dataObject.getPrimaryFile().getParent().getPath());
			JMenu main = new JMenu(Bundle.CTL_MakefileAction());
			JMenuItem menuItem = new JMenuItem("all");
			menuItem.addActionListener(this);
			main.add(menuItem);
			menuItem = new JMenuItem("clean");
			menuItem.addActionListener(this);
			main.add(menuItem);
			menuItem = new JMenuItem("run");
			menuItem.addActionListener(this);
			main.add(menuItem);
			menuItem = new JMenuItem("clean all run");
			menuItem.addActionListener(this);
			main.add(menuItem);
			return main;
		}
		return null;
	}

}
