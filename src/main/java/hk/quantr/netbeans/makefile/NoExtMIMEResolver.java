package hk.quantr.netbeans.makefile;

import org.openide.filesystems.FileObject;
import org.openide.filesystems.MIMEResolver;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
@ServiceProvider(service = MIMEResolver.class, position = 3214328)
public class NoExtMIMEResolver extends MIMEResolver {

	private static final String mimetype = "text/x-makefile";

	public NoExtMIMEResolver() {
		super(new String[]{mimetype});
	}

	@Override
	public String findMIMEType(FileObject fo) {
		if (!fo.canRead() || fo.isFolder()) {
			return null;
		}

		String name = fo.getName();
		if (name.equals("Makefile")) {
			return mimetype;
		}
		return null;
	}
}
